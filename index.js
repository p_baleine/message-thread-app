var app = require('./app'),
    config = require('config');

app.listen(config.port, function() {
  console.log('listening on %d.', config.port);
});
