var Bookshelf = require('bookshelf'),
    config = require('config').db.database,

    // initialize database
    blogBookshelf = Bookshelf.blogBookshelf = Bookshelf.initialize(config);

module.exports = {
  User: require('./user').User
};
