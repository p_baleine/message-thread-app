var Promise = require('bluebird'),
    bcrypt = Promise.promisifyAll(require('bcrypt')),
    Bookshelf = require('bookshelf').blogBookshelf;

// Uset model
var User = Bookshelf.Model.extend({

  tableName: 'users',

  hasTimestamps: true,

  initialize: function() {
    Bookshelf.Model.prototype.initialize.apply(this, arguments);

    // hash pasword on saveing
    this.on('saving', this.hashPassword, this);
  },

  // set hashed password
  hashPassword: function() {
    var _this = this;

    // generat salt
    return bcrypt.genSaltAsync(10)
      .then(function(salt) {
        _this.set('salt', salt);
        // hash password with the genrated salt
        return bcrypt.hashAsync(_this.get('password'), salt);
      })
      .then(function(hash) {
        return _this.set('password', hash);
      });
  }

}, {

  // authenticate user with `email` and `password`.
  authenticate: function(email, password) {
    // fetch user with `email`
    return new this({ email: email }).fetch({ require: true })
      .then(function(user) {
        // hash `password`
        return [bcrypt.hashAsync(password, user.get('salt')), user];
      })
      .spread(function(hash, user) {
        // compare hashed password to user's password
        // and return user if these passwords are equal
        if (hash === user.get('password')) { return user; }

        // throw an error if passwords are not equal.
        throw new Error('password or email is incorrect.');
      });
  }

});

// Usets collection
var Users = Bookshelf.Collection.extend({

  model: User

});

module.exports = {
  User: User,
  Users: Users
};
