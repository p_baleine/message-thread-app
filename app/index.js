var express = require('express'),
    Resource = require('express-resource'),
    session = require('./routes/session'),

    app = module.exports = express();

app.set('views', __dirname + '/views');
app.set('view engine', 'jade');

app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.cookieParser('your secret here'));
app.use(express.session());
app.use(app.router);
app.use(express.static(__dirname + '/../public'));

app.locals({
  title: 'My app'
});

app.get('/', function(req, res) {
  res.send('Hello message app');
});

app.resource('sessions', session);

