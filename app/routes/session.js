var User = require('../models').User;

// GET /new
exports.new = function(req, res) {
  res.render('session/new');
};

// POST /
exports.create = function(req, res, next) {
  var user = req.body.user;

  // authenticate with user's email and password
  User.authenticate(user.email, user.password)
    .then(function(user) {
      // if successfully authenticae user
      // set user's id to session and redirect to `/`
      req.session.uid = user.id;
      res.redirect('/');
    })
    .catch(function(e) {
      // if authentication failed
      // render session/new view with error
      req.session.uid = null;
      res.statusCode = 404;
      res.render('session/new', { error: [e] });
    });
};
