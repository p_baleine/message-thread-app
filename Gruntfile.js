var matchdep = require('matchdep');

module.exports = function(grunt) {
  matchdep.filterDev('grunt-*').forEach(grunt.loadNpmTasks);

  grunt.initConfig({
    watch: {
      unittest: {
        files: ['<%= mocha.all.src %>', 'app/**/*.js', 'app/**/*.jade'],
        tasks: ['unittest']
      },
      sass: {
        files: ['scss/**/*.scss'],
        tasks: ['sass']
      }
    },

    mocha: {
      options: {
        mochaAsPromised: true,
        chaiAsPromised: true,
        sinonChai: true
      },
      all: {
        src: ['./test/unit/**/*.js']
      }
    },

    clean: {
      unittest: {
        src: ['./db/message-thread-app.unittest.db']
      }
    },

    env: {
      development: {
        NODE_ENV: 'development'
      },
      unittest: {
        NODE_ENV: 'unittest'
      },
      alltest: {
        NODE_ENV: 'alltest'
      }
    },

    nodemon: {
      dev: {}
    },

    concurrent: {
      all: {
        tasks: ['nodemon', 'watch']
      },
      options: {
        logConcurrentOutput: true
      }
    },

    knexmigrate: {
      config: function(cb) {
        return cb(null, require('config').db);
      }
    },

    sass: {
      dev: {
        files: {
          'public/application.css': ['scss/message-thread-app.scss']
        }
      }
    }
  });

  grunt.loadTasks('tasks');

  grunt.registerTask('unittest', [
    'env:unittest',
    'clean',
    'knexmigrate:latest',
    'mocha'
  ]);

  grunt.registerTask('alltest', [
    'env:alltest',
    'clean',
    'knexmigrate:latest',
    'mocha'
  ]);

  grunt.registerTask('default', [
    'env:development',
    'sass:dev',
    'knexmigrate:latest',
    'concurrent'
  ]);
};
