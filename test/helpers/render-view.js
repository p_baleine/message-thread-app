var Promise = require('bluebird'),
    jade = require('jade'),
    jsdom = require('jsdom'),
    _ = require('lodash'),
    fs = require('fs'),

    option = { encoding: 'utf-8' },
    jquery = fs.readFileSync(__dirname + '/../../bower_components/jquery/jquery.min.js', option);

function renderView(fileName, local) {
  var deferred = Promise.defer(),
      html = jade.renderFile(fileName, _.extend({}, local));

  jsdom.env({
    html: html,
    src: [jquery],
    done: function(err, window) {
      if (err) {
        deferred.reject(err);
      } else {
        deferred.resolve(window);
      }
    }
  });

  return deferred.promise;
};

module.exports = renderView;
