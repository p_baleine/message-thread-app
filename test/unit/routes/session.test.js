var expect = require('chai').expect,
    Promise = require('bluebird'),
    sinon = require('sinon'),
    Bookshelf = require('bookshelf').blogBookshelf,
    session = require('../../../app/routes/session'),
    models = require('../../../app/models');

describe('session route', function() {
  beforeEach(function() {
    this.res = {
      render: sinon.spy(),
      redirect: sinon.spy()
    };
  });

  describe('#new()', function() {
    it('should be a function', function() {
      expect(session.new).to.be.a('function');
    });

    it('should render session/new view', function() {
      session.new(this.req, this.res);

      expect(this.res.render).to.have.been.calledWith('session/new');
    });
  });

  describe('#create()', function() {
    beforeEach(function() {
      var email = 'my@mail.com',
          password = 'mypass';

      this.userId = 100;
      this.req = { body: { user: { email: email, password: password } }, session: {} };
      this.user = new Bookshelf.Model({ id: this.userId, email: email, password: password });
      this.authenticateStub = sinon.stub(models.User, 'authenticate');
      this.authenticateStub.returns(Promise.resolve(this.user));
    });

    afterEach(function() {
      models.User.authenticate.restore();
    });

    it('should authenticate user with given email and password', function() {
      session.create(this.req, this.res);

      expect(this.authenticateStub)
        .to.have.been.calledWith(this.req.body.user.email, this.req.body.user.password);
    });

    describe('when successflly authenticate user', function() {
      it('should redirect to `/`', function() {
        session.create(this.req, this.res);

        process.nextTick(function() {
          expect(this.res.redirect).to.have.been.calledWith('/');
        }.bind(this));
      });

      it('should set session\'s uid', function() {
        session.create(this.req, this.res);

        process.nextTick(function() {
          expect(this.req.session.uid).to.equal(this.userId);
        }.bind(this));
      });
    });

    describe('when authentication failed', function() {
      beforeEach(function() {
        this.error = new Error('user not found');
        this.authenticateStub.returns(Promise.reject(this.error));
      });

      it('should set req.session.uid nil', function() {
        session.create(this.req, this.res);

        process.nextTick(function() {
          expect(this.req.session.uid).to.equal(null);
        }.bind(this));
      });

      it('should set res.statusCode 404', function() {
        session.create(this.req, this.res);

        process.nextTick(function() {
          expect(this.res.statusCode).to.equal(404);
        }.bind(this));
      });

      it('should render session/new with error message', function() {
        session.create(this.req, this.res);

        process.nextTick(function() {
          expect(this.res.render).to.have.been.calledWith('session/new');
          expect(this.res.render.lastCall.args[1]).to.have.property('error');
          expect(this.res.render.lastCall.args[1].error).to.include(this.error);
        }.bind(this));
      });
    });
  });
});
