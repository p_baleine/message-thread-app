var Promise = require('bluebird'),
    User = require('../../../app/models').User,
    expect = require('chai').expect;

describe('User', function() {
  it('should be a function', function() {
    expect(User).to.be.a('function');
  });

  describe('on saving', function() {
    it('should hash password', function() {
      var password = 'mypassword';

      return new User({ email: 'my@name.com', password: password, salt: 'aaa' }).save()
        .then(function(user) {
          return Promise.all([
            expect(user.get('password')).to.not.be.empty,
            expect(user.get('password')).to.not.equal(password),
            expect(user.get('salt')).to.not.be.empty
          ]);
        });
    });

    describe('validation', function() {
      it('should throw error if email is empty');
    });
  });

  describe('.authenticate()', function() {
    describe('when successfully authenticate given user', function() {
      it('should return authenticated user', function() {
        var email = 'my@name.com',
            password = 'mypassword';

        return new User({ email: email, password: password }).save()
          .then(function(user) {
            return User.authenticate(user.get('email'), password);
          })
          .then(function(user) {
            return expect(user).to.exist;
          });
      });
    });

    describe('when failed to authenticate given user', function() {
      it('should throw error');
    });
  });
});
