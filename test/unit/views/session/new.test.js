var expect = require('chai').expect,
    renderView = require('../../../helpers/render-view');

describe('session/create view', function() {
  function renderNewView(local) {
    return renderView(__dirname + '/../../../../app/views/session/new.jade', local);
  }

  it('should render form that has action pointing to `/sessions`', function() {
    return renderNewView().then(function(window) {
      return expect(window.$('form').attr('action')).to.equal('/sessions');
    });
  });

  it('should render form that has post method', function() {
    return renderNewView().then(function(window) {
      return expect(window.$('form').attr('method')).to.equal('post');
    });
  });

  it('should render email input field', function() {
    return renderNewView().then(function(window) {
      return expect(window.$('[name="user[email]"]')).to.have.length.of.at.least(1);
    });
  });

  it('should render password input field', function() {
    return renderNewView().then(function(window) {
      return expect(window.$('[name="user[password]"]')).to.have.length.of.at.least(1);
    });
  });

  describe('when rendered with error', function() {
    it('should render error message', function() {
      var errorMessage = 'email or password is incorrect';

      return renderNewView({ error: [new Error(errorMessage)] }).then(function(window) {
        return expect(window.$('.errors .item:first-child').text()).to.match(new RegExp(errorMessage));
      });
    });
  });
});
