module.exports = function(grunt) {
  grunt.registerMultiTask('mocha', 'run mocha', function() {
    var done = this.async(),

        // Aggregate file paths.
        args = this.files.reduce(function(memo, file) {
          return memo.concat(file.src);
        }, []);

    // apply growl if `--growl` option supplied
    if (grunt.option('growl')) { args.push('--growl'); }

    // Setup mocha-as-promised if `mochaAsPromised` option supplied
    if (this.options().mochaAsPromised) {
      args.push('--require=./tasks/lib/unittest-mocha-as-promised.js');
    }

    // Setup chai-as-promised if `chaiAsPromised` option supplied
    if (this.options().chaiAsPromised) {
      args.push('--require=./tasks/lib/unittest-chai-as-promised.js');
    }

    // Setup sinon.chai if `sinonChai` option supplied
    if (this.options().sinonChai) {
      args.push('--require=./tasks/lib/unittest-sinon-chai.js');
    }

    // spawn mocha process
    grunt.util.spawn({
      cmd: './node_modules/.bin/mocha',
      args: args,
      opts: {
        stdio: 'inherit'
      }
    }, function(error, result, cod) {
      if (error) {
        grunt.fail.fatal(result.stdout);
      }

      grunt.log.writeln(result.stdout);

      done();
    });
  });
};
